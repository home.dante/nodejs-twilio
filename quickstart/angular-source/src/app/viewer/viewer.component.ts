import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { connect, Room } from 'twilio-video';

/**
 * Get a {@link Location}'s query parameters.
 * @param {Location} location
 * @returns {Map<string, Array<string>>} queryParameters
 */
function getQueryParameters(location: Location): Map<string, Array<string>> {
  return (location.search.split('?')[1] || '').split('&').reduce((queryParameters, keyValuePair) => {
    let [key, value] = keyValuePair.split('=');
    key = decodeURIComponent(key);
    value = decodeURIComponent(value);
    queryParameters.set(key, (queryParameters.get(key) || []).concat([value]));
    return queryParameters;
  }, new Map());
}
@Component({
  selector: 'app-viewer',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.scss']
})
export class ViewerComponent implements OnInit {
  activeRoom;
  roomName="newroom";
  previewTracks;
  room;
  constructor(private api: ApiService) { }

  ngOnInit() {
    this.api.getToken().subscribe((res)=>{
      console.log(res.token);
      this.joinRoom(res.token)
    })
  }
// Attach the Tracks to the DOM.
 attachTracks(tracks, container) {
  tracks.forEach(function(track) {
    container.appendChild(track.attach());
  });
}

// Attach the Participant's Tracks to the DOM.
 attachParticipantTracks(participant, container) {
  var tracks = Array.from(participant.tracks.values());
  this.attachTracks(tracks, container);
}

// Detach the Tracks from the DOM.
 detachTracks(tracks) {
  tracks.forEach(function(track) {
    track.detach().forEach(function(detachedElement) {
      detachedElement.remove();
    });
  });
}

// Detach the Participant's Tracks from the DOM.
 detachParticipantTracks(participant) {
  var tracks = Array.from(participant.tracks.values());
  this.detachTracks(tracks);
}

  joinRoom(token){
 
    let roomName = this.roomName;
    if (!roomName) {
      alert('Please enter a room name.');
      return;
    }

    // log("Joining room '" + roomName + "'...");
    let connectOptions = {
      name: roomName,
      logLevel: 'debug'
    };

    if (this.previewTracks) {
      connectOptions['tracks'] = this.previewTracks;
    }

    // Join the Room with the token from the server and the
    // LocalParticipant's Tracks.
    connect(token).then(room => {
      this.room = room;
      room.once('disconnected', () => {});
      room.disconnect();
    }, error => {
      console.log(error)
    });
 }

// Successfully connected!
 roomJoined(room) {
   console.log("connecting")
   console.log("********room is", room)
  // window.room = activeRoom = room;

  // log("Joined as '" + identity + "'");
  // document.getElementById('button-join').style.display = 'none';
  // document.getElementById('button-leave').style.display = 'inline';

  // Attach LocalParticipant's Tracks, if not already attached.
  var previewContainer = document.getElementById('local-media');
  if (!previewContainer.querySelector('video')) {
    this.attachParticipantTracks(room.localParticipant, previewContainer);
  }

  // // Attach the Tracks of the Room's Participants.
  room.participants.forEach(function(participant) {
    // log("Already in Room: '" + participant.identity + "'");
    var previewContainer = document.getElementById('remote-media');
    this.attachParticipantTracks(participant, previewContainer);
  });

  // // When a Participant joins the Room, log the event.
  // room.on('participantConnected', function(participant) {
  //   log("Joining: '" + participant.identity + "'");
  // });

  // // When a Participant adds a Track, attach it to the DOM.
  // room.on('trackAdded', function(track, participant) {
  //   log(participant.identity + " added track: " + track.kind);
  //   var previewContainer = document.getElementById('remote-media');
  //   attachTracks([track], previewContainer);
  // });

  // // When a Participant removes a Track, detach it from the DOM.
  // room.on('trackRemoved', function(track, participant) {
  //   log(participant.identity + " removed track: " + track.kind);
  //   detachTracks([track]);
  // });

  // // When a Participant leaves the Room, detach its Tracks.
  // room.on('participantDisconnected', function(participant) {
  //   log("Participant '" + participant.identity + "' left the room");
  //   detachParticipantTracks(participant);
  // });

  // // Once the LocalParticipant leaves the room, detach the Tracks
  // // of all Participants, including that of the LocalParticipant.
  // room.on('disconnected', function() {
  //   log('Left');
  //   if (previewTracks) {
  //     previewTracks.forEach(function(track) {
  //       track.stop();
  //     });
  //     previewTracks = null;
  //   }
  //   detachParticipantTracks(room.localParticipant);
  //   room.participants.forEach(detachParticipantTracks);
  //   activeRoom = null;
  //   document.getElementById('button-join').style.display = 'inline';
  //   document.getElementById('button-leave').style.display = 'none';
  // });
}


  //leave Room.
  leaveRoom(){
    if(this.activeRoom)
    this.activeRoom.disconnect();
  }


  // Activity log.
 log(message) {
   console.log(message)
  // var logDiv = document.getElementById('log');
  // logDiv.innerHTML += '<p>&gt;&nbsp;' + message + '</p>';
  // logDiv.scrollTop = logDiv.scrollHeight;
}
}
