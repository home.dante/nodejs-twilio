import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';
// const SERVER_URL = "https://test-twilio-camera.herokuapp.com/"
const SERVER_URL = "http://localhost:3000/"

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  
  constructor(private http:HttpClient) { }

  getToken(){
    let url = `${SERVER_URL}token`; 
    return this.http.get<any>(url);
  }
}
